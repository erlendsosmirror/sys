/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int showrelocs (char **argv)
{
	int fd = open ("/dev/sys", O_RDWR);

	if (!fd)
	{
		PrintError ("sys", "/dev/sys");
		return errno;
	}

	int ret = ioctl (fd, SYSSHOWREL, (int) argv[2]);

	close (fd);

	if (ret)
	{
		PrintError ("sys", "/dev/sys");
		return errno;
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Usage
///////////////////////////////////////////////////////////////////////////////
int PrintUsage (void)
{
	SimplePrint ("Usage: sys [options]\n"
		   "  showrelocs <tty>\n"
		   "    Print load info to tty\n"
		   "    Off when tty is not given\n"
		   "  --help\n"
		   "    Print this message\n");
	return 0;
}

int PrintUnknownCommand (void)
{
	SimplePrint (
		"Invalid argument, use\n"
		"   sys --help\n"
		"for a usage list\n");
	return 1;
}


///////////////////////////////////////////////////////////////////////////////
// Main function
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	// If no arguments, print default
	if (argc == 1)
		return PrintUnknownCommand ();

	// Check arguments
	if (!strcmp (argv[1], "showrelocs"))
		return showrelocs (argv);
	else if (!strcmp(argv[1], "--help"))
		return PrintUsage ();
	else
		return PrintUnknownCommand ();
}
